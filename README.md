# Terra Nova
## Projekt iz kolegija 3D računalna grafika
#### - izradio Antonio Janjić

![mainimg](./git-images/main_image.png)

Projekt je demonstracija tehnika koje se koriste za izradu proceduralnog terena i okoline. 

Glavni dio softvera je scena koja prikazuje teren izgrađen koristeći OpenSimplex noise koji se interaktivno može mijenjati uz pomoć raznih parametara te istraživati s potpuno pokretnom kamerom.

### Tehnički detalji
Projekt je napravljen koristeći OpenGL; u mini engineu kojeg sam napravio po uzoru na implementacije Yan Chernikova.
Inspiracija za načine implementiranja proceduralne generacije dolaze od Sebastian Lagueovih videja na tu temu.

## Reference
[**Yan Chernikov's (TheCherno)**](https://twitter.com/thecherno) [*Youtube*](https://www.youtube.com/user/TheChernoProject) [*Github*](https://github.com/TheCherno/) **OpenGL** i **Game Engine** series 

[**Sebastian Lague**](https://twitter.com/sebastianlague) [*Youtube*](https://www.youtube.com/c/SebastianLague) [*Github*](https://github.com/SebLague) **Unity Procedural Landmass Generation** i **Procedural Planets** series

## 3rd party
* [GLEW](http://glew.sourceforge.net/)
* [GLFW](https://www.glfw.org/)
* [GLM](https://glm.g-truc.net/)
* [stb_image](https://github.com/nothings/stb)
* [Dear ImGui](https://github.com/ocornut/imgui)
* [FastNoiseLite](https://github.com/Auburn/FastNoiseLite)
* [chromium/cubicbezier](https://chromium.googlesource.com/chromium/+/refs/heads/trunk)

## Assets
* Unity standard asset library
* [TextureHaven](https://texturehaven.com/)
* [Poliigon](https://www.poliigon.com)
