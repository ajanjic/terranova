#shader vertex
#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex;

out DATA
{
	vec2 tex;
	float light;
} frag

uniform mat4 u_projMat = mat4(1);
uniform mat4 u_viewMat = mat4(1);
uniform mat4 u_modelMat = mat4(1);

const vec3 lightDir = vec3(0, -1, 0);

void main()
{
	frag.tex = texCoord;
	frag.light = -dot(normal, lightDir);
	if (v_light > 1) v_light = 1;
	if (v_light < 0) v_light = 0;
	gl_Position = u_projMat * u_viewMat * u_modelMat * vec4(position, 1.0);
};


#shader fragment
#version 330 core

layout(location = 0) out vec4 out_color;

in DATA
{
	vec2 tex;
	float light;
} frag

uniform sampler2D u_texture;

void main()
{
	//color = vec4(vec3(texture(u_texture, v_texCoord) * v_light),1.0f);
	vec3 colorSolid = vec4(vec3(1, 1, 1) * frag.light,1.0f);
	vec3 colorTex = texture(u_texture, frag.tex).rgb;
	out_color = vec4(colorTex, 1.0f);
}