#shader vertex
#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex;


uniform mat4 u_projMat;
uniform mat4 u_viewMat;
uniform mat4 u_modelMat = mat4(1);

out vec2 frag_tex;

void main()
{
	frag_tex = tex;
	gl_Position = u_projMat * mat4(mat3(u_viewMat)) * u_modelMat * vec4(pos, 1.0);
}

#shader fragment
#version 330 core

uniform sampler2D u_cloudmap;
uniform float u_sunAngle;

in vec2 frag_tex;
out vec4 fragColor;

void main()
{
	float time = 1.0f - (abs(u_sunAngle/90 - 1.0f))* (abs(u_sunAngle / 90 - 1.0f));	//edit time  make it so that near the edges the clouds get darker

	float colorIntensity = texture(u_cloudmap, frag_tex).r;

	fragColor = vec4(vec3(1.0f, 1.0f, 1.0f) * time, 1.0 * colorIntensity);
}