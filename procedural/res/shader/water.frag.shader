#shader fragment
#version 330 core

layout(location = 0) out vec4 out_color;

in DATA{
	vec3 pos;
	vec2 tex;
} frag;

uniform vec3 u_sunPos;
uniform float u_sunAngle;
uniform vec3 u_camPos;
uniform vec2 u_offset = vec2(0,0);

uniform sampler2D u_texture;
uniform sampler2D u_skymap;
uniform sampler2D u_normalmap;

void main()
{
	float time = clamp(pow(u_sunAngle / 90.0f - 1, 4) + 0.01, 0.0f, 1.0f);
	vec4 ambientColor = texture(u_skymap, vec2(time, 0.5));

	vec3 normal = texture(u_normalmap, frag.tex + u_offset).rbg;	//channels swapped due to plane pointing +y
	normal = normalize(normal * 2.0 - 1.0);

	vec4 texColor = texture(u_texture, frag.tex);

	vec3 lightDir = normalize(u_sunPos - frag.pos);
	vec4 diffuse = texColor * max(dot(normal, lightDir), 0.0) * 0.8;

	vec3 viewDir = normalize(u_camPos - frag.pos);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	vec4 specular = texColor * pow(max(dot(normal, halfwayDir), 0.0), 1000.0f);

	out_color = mix(diffuse + specular, (diffuse + specular) * ambientColor, time);
}