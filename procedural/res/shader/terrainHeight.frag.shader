#shader fragment
#version 330 core

in DATA{
	vec3 pos;
	vec3 normal;
	vec2 tex;
} frag;

uniform float u_minHeight = 0.0f;
uniform float u_maxHeight = 1.0f;

layout(location = 0) out vec4 out_color;

float inverseLerp(float minv, float maxv, float v)	//inverse linear interpolation, for minv and maxv return a value in [0,1]
{
	return clamp((v - minv) / (maxv-minv), 0, 1);
}

void main()
{
	out_color = vec4(vec3(1.0, 1.0, 1.0) * inverseLerp(u_minHeight, u_maxHeight, frag.pos.y), 1.0f);
}