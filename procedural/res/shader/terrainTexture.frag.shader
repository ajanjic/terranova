#shader fragment
#version 330 core

const float epsilon = 0.0001;
const int maxTextures = 16;

in DATA{
	vec3 pos;
	vec3 normal;
	vec2 tex;
} frag;

uniform float u_scale = 512.0f;
uniform float u_minHeight = 0.0f;
uniform float u_maxHeight = 1.0f;
uniform float u_sunAngle;
uniform vec3 u_sunPosition = vec3(10, 1000, 0);

uniform int u_colorsOnly = 0;

uniform sampler2DArray u_texture;
uniform int u_texNum = 0;
uniform float u_texValues[maxTextures];
uniform float u_blendAmounts[maxTextures];

uniform sampler2D u_skymap;

layout(location = 0) out vec4 out_color;

float inverseLerp(float minv, float maxv, float v)	//inverse linear interpolation, for minv and maxv return a value in [0,1]
{
	return clamp((v - minv) / (maxv-minv), 0, 1);
}

vec3 triplanar(vec3 pos, vec3 blendAxis, int textureIndex)	//triplanar mapping - Sebastian Lague
{
	vec3 scaledPos = pos / u_scale;
	vec3 xProjection = (texture(u_texture, vec3(scaledPos.yz, textureIndex)) * blendAxis.x).rgb;
	vec3 yProjection = (texture(u_texture, vec3(scaledPos.xz, textureIndex)) * blendAxis.y).rgb;
	vec3 zProjection = (texture(u_texture, vec3(scaledPos.xy, textureIndex)) * blendAxis.z).rgb;
	return xProjection + yProjection + zProjection;
}

void main()
{
	float time = clamp(pow(u_sunAngle / 90.0f - 1, 4) + 0.01, 0.0f, 1.0f);
	vec3 ambientColor = texture(u_skymap, vec2(time, 0.5)).rgb;

	vec3 lightDir = normalize(u_sunPosition - frag.pos);
	float lightIntensity = max(dot(frag.normal, lightDir), 0.0f);

	float fragY = inverseLerp(u_minHeight, u_maxHeight, frag.pos.y);

	vec3 blendAxis = normalize(abs(frag.normal));
	blendAxis /= blendAxis.x + blendAxis.y + blendAxis.z;	//ensure that the texture color doesn't exceed 1.0f

	vec3 color = vec3(0.0f);

	for (int i = 0; i < u_texNum; i++)
	{
		float intensity = inverseLerp(-u_blendAmounts[i] / 2 - epsilon, u_blendAmounts[i] / 2, fragY - u_texValues[i]);

		vec3 texColor = triplanar(frag.pos, blendAxis, i);

		if (u_colorsOnly == 1)
		{
			texColor = texture(u_texture, vec3(0.4, 0.4, i)).rgb;
		}
		
		color = mix(color, texColor, intensity);
	}

	out_color = vec4(mix(color * lightIntensity, color * (ambientColor / 2), time), 1.0);
}