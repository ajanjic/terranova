#shader vertex
#version 330 core
layout(location = 0) in vec3 pos;

uniform mat4 u_projMat;
uniform mat4 u_viewMat;
uniform mat4 u_modelMat = mat4(1);

out vec3 fragpos;
void main()
{
	gl_Position = u_projMat * mat4(mat3(u_viewMat)) * u_modelMat * vec4(pos, 1.0);
	fragpos = vec3(u_modelMat * vec4(pos, 1.0));
}

#shader fragment
#version 330 core


uniform sampler2D u_skymap;
uniform float u_sunAngle;
uniform vec3 u_sunVec;

in vec3 fragpos;
out vec4 out_color;

void main()
{
	float norm = length(fragpos);
	float yValue = fragpos.y / norm;
	float time = u_sunAngle / 180.0f;

	float angle = abs(acos(dot(fragpos, u_sunVec) / (length(fragpos) * length(u_sunVec))));

	vec4 bgColor = texture(u_skymap, vec2(time, yValue + 0.5));

	float intens = 0;
	if (angle < 0.075)
	{
		out_color = vec4(1.0f, 1.0f * (bgColor.b * 3), 1.0f * bgColor.b, 1.0f);
	}
	else
	{
		out_color = bgColor;
	}


}