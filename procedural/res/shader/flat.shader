#shader vertex
#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

out vec2 v_texCoord;

uniform mat4 u_projMat = mat4(1);
uniform mat4 u_viewMat = mat4(1);
uniform mat4 u_modelMat = mat4(1);

void main()
{
	gl_Position = u_projMat * u_viewMat * u_modelMat * vec4(position, 1.0);
};


#shader fragment
#version 330 core

layout(location = 0) out vec4 color;

void main()
{
	color = vec4(0.8, 0.8, 0.8, 1.0);
};