#shader vertex
#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex;

out DATA{
	vec3 pos;
	vec2 tex;
} frag;

uniform mat4 u_projMat = mat4(1);
uniform mat4 u_viewMat = mat4(1);
uniform mat4 u_scaleMat = mat4(1);
uniform mat4 u_centerMat = mat4(1);
uniform mat4 u_rotMat = mat4(1);


void main()
{
	frag.tex = tex;
	frag.pos = (u_scaleMat * u_centerMat * u_rotMat * vec4(pos, 1.0)).xyz;

	gl_Position = u_projMat * u_viewMat * u_scaleMat * u_centerMat * u_rotMat * vec4(pos, 1.0);
};