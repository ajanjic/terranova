#shader vertex
#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex;

out DATA{
	vec3 pos;
	vec3 normal;
	vec2 tex;
} frag;

uniform mat4 u_projMat = mat4(1);
uniform mat4 u_viewMat = mat4(1);
uniform mat4 u_modelMat = mat4(1);

void main()
{
	frag.pos = vec3(u_modelMat * vec4(pos, 1.0));
	frag.normal = vec3(transpose(inverse(u_modelMat)) * vec4(normal, 1.0));
	frag.tex = tex;
	gl_Position = u_projMat * u_viewMat * u_modelMat * vec4(pos, 1.0);
};