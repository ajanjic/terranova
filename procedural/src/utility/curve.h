#pragma once
#include <array>
#include <vector>
#include <string>
#include "glm/glm.hpp"
#include "imgui/imgui.h"
#include "cubicbezier.h"
#include "util.h"

const int c_curveSamples = 16;

class Curve
{
private:
	std::array<glm::vec2, 2> m_p;

	CubicBezier* bezier = nullptr;
public:
	Curve(glm::vec2 p1, glm::vec2 p2);

	void setCtrlPoint(glm::vec2 val, unsigned int index);
	glm::vec2 getValue(float v);
	float getY(float x);

	bool onImGuiRender();
};