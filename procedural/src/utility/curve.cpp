#include "curve.h"

Curve::Curve(glm::vec2 p1, glm::vec2 p2)
	: m_p{p1, p2}
{
	bezier = new CubicBezier(p1.x, p1.y, p2.x, p2.y);
}

glm::vec2 Curve::getValue(float v)
{
	return {bezier->SampleCurveX(v), bezier->SampleCurveY(v)};
}

float Curve::getY(float x)
{
	return bezier->Solve(x);
}

bool Curve::onImGuiRender()
{
	const ImU32 col = util::toHEX({0.7f,0.7f,0.7f, 1.0f});
	const ImU32 colbg = util::toHEX({0.3f,0.3f,0.3f, 1.0f});
	const int subdivNum = 16;


	bool ret = false;
	for (int i = 0; i < m_p.size(); i++)
	{
		ImGui::SliderFloat2(std::to_string(i).c_str(), &m_p[i].x, 0.0f, 1.0f, "%.2f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			delete bezier;
			bezier = new CubicBezier(m_p[0].x, m_p[0].y, m_p[1].x, m_p[1].y);
			ret = true;
		}
	}

	//render curve code
	ImDrawList* draw_list = ImGui::GetWindowDrawList();

	static float sz = 128.0f;

	ImVec2 offset = ImGui::GetCursorScreenPos();

	for (int i = 0; i <= 4; i++)	//draw horizontal lines
	{
		draw_list->AddLine(ImVec2(offset.x, offset.y + (sz / 4) * i), ImVec2(offset.x + sz, offset.y + (sz / 4) * i), colbg, 0.5f);
	}

	std::vector<ImVec2> lineSegs;
	lineSegs.reserve(subdivNum + 1);
	for (int i = 0; i <= subdivNum; i++)
	{
		float sample = ((float)i) / subdivNum;
		glm::vec2 val = getValue(sample);
		lineSegs.emplace_back(offset.x + val.x * sz, offset.y + (1 - val.y) * sz);
	}
	for (int i = 0; i < lineSegs.size() - 1; i++)
	{
		draw_list->AddLine(lineSegs[i], lineSegs[i + 1], col, 1.0f);
	}


	return ret;
}