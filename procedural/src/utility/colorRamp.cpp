#include "colorRamp.h"

void ColorRamp::addElement(float value, float blendAmt, std::string name, glm::vec3 color)
{
	m_ramp.push_back({value, blendAmt, name, color});
	std::sort(m_ramp.begin(), m_ramp.end());
}

bool ColorRamp::onImGuiRender()
{
	//ImGui::Begin("Color Ramp");
	bool ret = false;

	for (int i = 0; i < m_ramp.size(); i++)
	{
		ImGui::PushID(i);

		ImGui::DragFloat(m_ramp[i].name.c_str(), &m_ramp[i].value, 0.01f, 0.001f, 1.0f, "%.2f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			std::sort(m_ramp.begin(), m_ramp.end());
			ret = true;
		}
		ImGui::DragFloat("blend amount", &m_ramp[i].blendAmount, 0.01f, 0.0f, 1.0f, "%.2f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			ret = true;
		}
		ImGui::ColorEdit3("", &m_ramp[i].color.r, ImGuiColorEditFlags_NoDragDrop | ImGuiColorEditFlags_PickerHueWheel);
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			ret = true;
		}

		
		ImGui::PopID();
		ImGui::NewLine();
	}
	ImGui::NewLine();

	ImGui::InputText("New color name", m_newColorName, 50, ImGuiInputTextFlags_EnterReturnsTrue);
	if(ImGui::Button("Add color"))
	{
		addElement(1.0f, 0.0f, m_newColorName, {1.0f,1.0f,1.0f});
		m_newColorName[0] = '\0';
		ret = true;
	}

	return ret;
	//ImGui::End();
}
