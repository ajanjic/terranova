#pragma once
#include <iostream>
#include "glm/glm.hpp"

#ifdef _DEBUG
#define DEBUGCALL(x) x
#define ASSERT_MSG(x, msg) if (!(x)) {\
	std::cout << "ASSERT: " <<msg << std::endl;\
	__debugbreak();}
#define ASSERT(x) if (!(x)) {__debugbreak();}
#else
#define DEBUGCALL(x)

#define ASSERT_MSG(x,msg)
#define ASSERT(x)
#endif
namespace util
{

	static glm::vec3 toRGB(unsigned int hex)
	{
		glm::vec3 ret;
		ret.r = ((float)((hex & (0xff << 16)) >> 16)) / 255;
		ret.g = ((float)((hex & (0xff << 8)) >> 8)) / 255;
		ret.b = ((float)(hex & 0xff)) / 255;

		return ret;
	}

	static glm::vec4 toRGBA(unsigned int hex)
	{
		glm::vec4 ret;
		ret.r = ((float)((hex & (0xff << 24)) >> 24)) / 255;
		ret.g = ((float)((hex & (0xff << 16)) >> 16)) / 255;
		ret.b = ((float)((hex & 0xff << 8) >> 8)) / 255;
		ret.a = ((float)(hex & 0xff)) / 255;

		return ret;
	}

	static unsigned int toHEX(glm::vec3 rgb)
	{
		unsigned int ret = 0;
		ret |= ((unsigned char)(rgb.r * 255) & 0xff) << 16;
		ret |= ((unsigned char)(rgb.g * 255) & 0xff) << 8;
		ret |= ((unsigned char)(rgb.b * 255) & 0xff);

		return ret;
	}

	static unsigned int toHEX(glm::vec4 rgba)
	{
		unsigned int ret = 0;
		ret |= ((unsigned char)(rgba.r * 255) & 0xff) << 24;
		ret |= ((unsigned char)(rgba.g * 255) & 0xff) << 16;
		ret |= ((unsigned char)(rgba.b * 255) & 0xff) << 8;
		ret |= ((unsigned char)(rgba.a * 255) & 0xff);

		return ret;
	}

	static float sampleBitmap(int w, int h, float x, float y, const float* map)
	{
		return map[(unsigned int)((y * h) * w + (x * w))];
	}
}