#pragma once
#include <vector>
#include <string>
#include <utility>
#include <algorithm>
#include "imgui/imgui.h"
#include "glm/glm.hpp"

struct ColorRampElement
{
	float value;
	float blendAmount;
	std::string name;
	glm::vec3 color;

	bool operator<(const ColorRampElement& oth)
	{
		return (this->value < oth.value);
	}
};

class ColorRamp
{
private:
	std::vector<ColorRampElement> m_ramp;
	char m_newColorName[50] = "\0";

public:
	void addElement(float value, float belndAmt, std::string name, glm::vec3 color);

	const std::vector<ColorRampElement>& getRampData() const { return m_ramp; }

	bool onImGuiRender();
};