#include "textureMath.h"


void TextureMath::calc2(float* target, const float* oth, int w0, int h0, const std::function<float(float, float)>& fnc)
{
	for (int i = 0; i < h0; i++)
	{
		for (int j = 0; j < w0; j++)
		{
			float val = fnc(target[i * w0 + j], oth[i * w0 + j]);

			if (val > 1.0f) val = 1.0f;
			if (val < 0.0f) val = 0.0f;

			target[i * w0 + j] = val;
		}
	}
}

void TextureMath::calc2(float* target, const float* oth, int w0, int h0, int w1, int h1, const std::function<float(float, float)>& fnc)
{
	for (int i = 0; i < h0; i++)
	{
		for (int j = 0; j < w0; j++)
		{
			float x = (float)j / w0;
			float y = (float)i / h0;

			float val = fnc(target[i * w0 + j], util::sampleBitmap(w1, h1, x, y, oth));

			if (val > 1.0f) val = 1.0f;
			if (val < 0.0f) val = 0.0f;

			target[i * w0 + j] = val;
		}
	}
}

void TextureMath::calc(float* target, int w, int h, const std::function<float(float)>& fnc)
{
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
		{
			float val = fnc(target[i * w + j]);

			if (val > 1.0f) val = 1.0f;
			if (val < 0.0f) val = 0.0f;

			target[i * w + j] = val;
		}
	}
}

void TextureMath::calc2(NoiseTexture& target, const NoiseTexture& oth, const std::function<float(float, float)>& fnc)
{
	calc2(target.getHeightMap(), oth.getHeightMap(), target.getWidth(), target.getHeight(), oth.getWidth(), oth.getHeight(), fnc);
}
void TextureMath::calc2(NoiseTexture& target, const FalloffTexture& oth, const std::function<float(float, float)>& fnc)
{
	calc2(target.getHeightMap(), oth.getData(), target.getWidth(), target.getHeight(), oth.getWidth(), oth.getHeight(), fnc);
}
void TextureMath::calc(NoiseTexture& target, const std::function<float(float)>& fnc)
{
	calc(target.getHeightMap(), target.getWidth(), target.getHeight(), fnc);
}