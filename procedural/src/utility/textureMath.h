#pragma once
#include <functional>
#include <cmath>
#include "noiseTexture.h"
#include "falloffTexture.h"

class TextureMath
{
public:
	struct Function
	{
		static inline float add(float a, float b) { return a + b; }
		static inline float sub(float a, float b) { return a - b; }
		static inline float mult(float a, float b) { return a * b; }
		static inline float min(float a, float b) { return (a < b) ? a : b; }
		static inline float max(float a, float b) { return (a > b) ? a : b; }
		static inline float floorfnc(float a) { return floor(a); }
		static inline float roundfnc(float a) { return round(a); }
		static inline float ceilfnc(float a) { return ceil(a); }
	};

	//all values are clamped [0,1]
	static void calc2(float* target, const float* oth, int w, int h, const std::function<float(float, float)>& fnc);
	static void calc2(float* target, const float* oth, int w0, int h0, int w1, int h1, const std::function<float(float, float)>& fnc);

	static void calc(float* target, int w, int h, const std::function<float(float)>& fnc);

	static void calc2(NoiseTexture& target, const NoiseTexture& oth, const std::function<float(float, float)>& fnc);
	static void calc2(NoiseTexture& target, const FalloffTexture& oth, const std::function<float(float, float)>& fnc);
	static void calc(NoiseTexture& target, const std::function<float(float)>& fnc);
};