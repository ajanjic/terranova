#pragma once
#include "vertexarray.h"
#include "indexbuffer.h"
#include "primitives.h"
#include <vector>
#include <memory>
#include <glm/glm.hpp>

class Object
{
public:
	Object() {}
	virtual ~Object() {};
	virtual const VertexArray& getVA() = 0;
	virtual const IndexBuffer& getIB() = 0;
};

class Quad : public Object
{
private:
	std::vector<Vertex> data =
	{
		{{0.0f,0.0f,0.0f}, {0.0f,0.0f,1.0f}, {0.0f,1.0f}},
		{{1.0f,0.0f,0.0f}, {0.0f,0.0f,1.0f}, {1.0f,1.0f}},
		{{0.0f,1.0f,0.0f}, {0.0f,0.0f,1.0f}, {0.0f,0.0f}},
		{{1.0f,1.0f,0.0f}, {0.0f,0.0f,1.0f}, {1.0f,0.0f}}
	};
	std::vector<glm::u32vec3> indicies =
	{
		{0,1,2},
		{1,2,3}
	};
	VertexBuffer* m_vb;
	IndexBuffer* m_ib;
	VertexBufferLayout* m_layout;

	VertexArray* m_va;
public:
	Quad() : m_vb(nullptr), m_ib(nullptr), m_layout(new VertexBufferLayout()), m_va(new VertexArray())
	{
		m_vb = new VertexBuffer(&(data[0].position.x), (unsigned int)(data.size() * sizeof(Vertex)));
		m_ib = new IndexBuffer(&(indicies[0].x), (unsigned int)(indicies.size() * 3));

		m_layout->push<Vertex>(1);
		m_va->addBuffer(*m_vb, *m_layout);
	}
	~Quad()
	{
		delete m_vb;
		delete m_ib;
		delete m_layout;
		delete m_va;
	}

	inline const VertexArray& getVA() { return *m_va; }
	inline const IndexBuffer& getIB() { return *m_ib; }
};