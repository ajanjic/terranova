#pragma once
#include "openGLApp.h"
#include "object.h"
#include "renderer.h"
#include "shader.h"
#include "texture.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"

#include "utility/globals.h"
#include "noiseTexture.h"
#include "utility/textureMath.h"

class NoiseTextureTest : public OpenGLApp
{
private:
	Shader m_shader;

	glm::mat4 m_proj = glm::ortho(-0.5, 1.5, -0.5 * c_height / c_width, 1.5 * c_height / c_width, -10.0, 10.0);
	Quad quad;

	unsigned char* dataPtr = nullptr;

	glm::vec3 m_offset = {0.0f,0.0f,0.0f};

	NoiseTexture m_noiseTexture;
	NoiseTexture m_noiseTexture2;
	FalloffTexture m_falloffTexture;

	float m_coeff = 0.6f;

	Texture* m_texture = nullptr;
public:
	NoiseTextureTest();
	~NoiseTextureTest();

	void appInit() override;
	//void imGuiInit() override;
	void onRender() override;
	void onImGuiRender() override;

	void onNoiseGen();
};