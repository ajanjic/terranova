#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_opengl3.h>
#include <imgui/imgui_impl_glfw.h>
#include <vector>
#include <string>
#include <functional>
#include <utility>
#include <iostream>

class OpenGLApp
{
public:
	OpenGLApp() {}
	virtual ~OpenGLApp() {}

	virtual void setCallbacks(GLFWwindow* window) {}
	virtual void appInit() {}
	virtual void onRender() {}
	virtual void onImGuiRender() {}
	virtual void inputHandle(GLFWwindow* window) {}
};


class AppMainMenu
	: public OpenGLApp
{
private:
	std::vector<std::pair<std::string, std::function<OpenGLApp* ()>>> m_apps;

public:
	OpenGLApp*& m_currentApp;
	GLFWwindow*& m_window;

	AppMainMenu(OpenGLApp*& currentAppPointer, GLFWwindow*& windowPointer) : m_currentApp(currentAppPointer), m_window(windowPointer) {}

	void onRender() override { glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); }
	void onImGuiRender() override
	{
		{
			ImGui::Begin("Main Menu");
			for (int i = 0; i < (int)m_apps.size(); i++)
			{
				ImGui::PushID(i);
				if (ImGui::Button(m_apps[i].first.c_str()))
				{
					m_currentApp = m_apps[i].second();
					m_currentApp->setCallbacks(m_window);
					m_currentApp->appInit();
				}
				ImGui::PopID();
			}
			ImGui::End();
		}
	}

	template<typename T>
	void registerApp(const std::string& name)
	{
		std::cout << "Registering test: " << name << std::endl;
		m_apps.push_back(std::make_pair(name, []() {return new T(); }));
	}
};