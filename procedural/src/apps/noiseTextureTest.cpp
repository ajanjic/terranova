#include "noiseTextureTest.h"


NoiseTextureTest::NoiseTextureTest() : m_shader("res/shader/noiseTexture.shader"), m_noiseTexture(128, 128), m_noiseTexture2(128, 128), m_falloffTexture(128,128)
{
}
NoiseTextureTest::~NoiseTextureTest()
{
}

void NoiseTextureTest::appInit()
{
	m_noiseTexture.setTextureCallback(std::bind(&NoiseTextureTest::onNoiseGen, this));

	m_noiseTexture.generate();
	m_noiseTexture2.generate();
	m_texture = m_noiseTexture.getTexture();
}
//void NoiseTextureTest::imGuiInit() {}

void NoiseTextureTest::onRender()
{
	Renderer::clear();

	if(m_texture)
		m_texture->bind();

	//Texture texture("res/tex/dbg_16x16.png");

	m_shader.bind();
	m_shader.setUniform1i("u_texture", 0);
	m_shader.setUniformMat4f("u_projMat", m_proj);

	Renderer::draw(quad.getVA(), quad.getIB(), m_shader);
}

void NoiseTextureTest::onImGuiRender()
{
	ImGui::Begin("Noise Texture 1");
	m_noiseTexture.onImGuiRender();
	ImGui::End();
	ImGui::Begin("Noise Texture 2");
	m_noiseTexture2.onImGuiRender();
	ImGui::End();

	ImGui::Begin("Debug");
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

	if (ImGui::Button("add"))
	{
		TextureMath::calc2(m_noiseTexture, m_noiseTexture2, TextureMath::Function::add);
		m_noiseTexture.generate(false);
	}
	if (ImGui::Button("sub"))
	{
		TextureMath::calc2(m_noiseTexture, m_noiseTexture2, TextureMath::Function::sub);
		m_noiseTexture.generate(false);
	}
	if (ImGui::Button("mult"))
	{
		TextureMath::calc2(m_noiseTexture, m_noiseTexture2, TextureMath::Function::mult);
		m_noiseTexture.generate(false);
	}
	ImGui::End();

	ImGui::Begin("Falloff Texture");
	m_falloffTexture.onImGuiRender();
	
	ImGui::End();
}

void NoiseTextureTest::onNoiseGen()
{
	m_texture = m_noiseTexture.getTexture();

}