#pragma once
#include "object.h"
#include "shader.h"

#include "glm/gtc/matrix_transform.hpp"
#include "utility/curve.h"
#include "noiseTexture.h"
#include "falloffTexture.h"
#include "utility/textureMath.h"

class Terrain : public Object
{
private:
	int m_size;

	Curve m_curve;	//interpolation curve used for determining the height values of the terrain
	float m_heightMultiplier;

	
	std::vector<Vertex> m_vertices;
	std::vector<glm::u32vec3> m_faces;

	VertexArray m_VA;
	IndexBuffer* m_IB = nullptr;
	VertexBuffer* m_VB = nullptr;
	VertexBufferLayout m_layout;

	
	void m_generateMesh();

	int m_modFunction = 0;
public:
	Terrain(int size = 128, float heightMult = 85.0f);
	~Terrain();

	NoiseTexture modTexture;
	NoiseTexture noiseTexture;
	FalloffTexture falloffTexture;
	Shader m_heightShader;
	Shader m_textureShader;

	glm::mat4 translationMat;

	inline VertexArray& getVA() {return m_VA;}
	inline IndexBuffer& getIB() {return *m_IB;}
	
	void adjustHeight();
	void calculateNormals();

	float heightFnc(float v);

	void changeSize(int size);

	inline int getSize() const { return m_size; }

private:
	bool m_autoUpdate = true;

	bool m_noiseWindow = false;
	bool m_falloffWindow = false;
	bool m_showModTexture = false;

	int m_sizeAux;
	bool m_useFalloff = true;
	bool m_useModTexture = false;

	bool m_usingMult = true;
	bool m_usingSub = false;
	bool m_usingAdd = false;
public:
	bool onImGuiRender();
};