#pragma once
#include "gllog.h"
#include <vector>
#include <string>
#include "stb/stb_image.h"
#include "imgui/imgui.h"

struct Texture3DElement
{
	float value;
	float blendAmount;
	std::string name;
};

class Texture3D
{
private:
	unsigned int m_rendererID;
	int m_width, m_height;
	int m_layerNum;
	float m_scale = 30.0f;

public:
	Texture3D(const std::vector<std::string>& filepaths, const std::vector<Texture3DElement>& props);
	~Texture3D();

	std::vector<Texture3DElement> properties;

	void bind(unsigned int slot = 0) const;
	void unbind() const;

	inline unsigned int getID() const { return m_rendererID; }
	inline float getScale() const { return m_scale; }

	bool onImGuiRender();
};