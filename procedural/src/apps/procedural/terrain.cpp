#include "terrain.h"

Terrain::Terrain(int size /*= 128*/, float heightMult /*= 85.0f*/)
	: m_size(size), noiseTexture(size, size), modTexture(size, size), falloffTexture(size, size, 8.0f, 1.4f), m_curve({{0.8,0.44}, {0.64,0.53}}), m_heightMultiplier(heightMult), m_layout(), m_sizeAux(size),
	m_heightShader("./res/shader/terrain.vert.shader", "./res/shader/terrainHeight.frag.shader"),
	m_textureShader("./res/shader/terrain.vert.shader", "./res/shader/terrainTexture.frag.shader")
{
	modTexture.generate();
	noiseTexture.generate();
	TextureMath::calc2(noiseTexture, falloffTexture, TextureMath::Function::sub);

	m_layout.push<Vertex>(1);

	m_generateMesh();

	noiseTexture.setTextureCallback(
		[this]() {
			if (m_useFalloff)
			{
				TextureMath::calc2(noiseTexture, falloffTexture, TextureMath::Function::sub);
			}
			if (m_autoUpdate) adjustHeight();
		}
	);

	falloffTexture.setTextureCallback(
		[this]() {
			if (m_useFalloff)
			{
				noiseTexture.generate(true, false);
				TextureMath::calc2(noiseTexture, falloffTexture, TextureMath::Function::sub);
			}
			if (m_autoUpdate) adjustHeight();
		}
	);
}

void Terrain::m_generateMesh()
{
	m_vertices.resize(m_size * m_size);
	m_faces.resize((m_size - 1) * (m_size - 1) * 2);

	float minHeight = std::numeric_limits<float>::max();
	float maxHeight = std::numeric_limits<float>::min();

	float increment = 1.0f / m_size;
	for (int i = 0; i < m_size; i++)
	{
		for (int j = 0; j < m_size; j++)
		{
			Vertex& vert = m_vertices[i * m_size + j];

			vert.position.x = (float)j * 2 - m_size;
			vert.position.z = (float)i * 2 - m_size;
			float x = increment * j;
			float y = increment * i;

			float sample = util::sampleBitmap(noiseTexture.getWidth(), noiseTexture.getHeight(), x, y, noiseTexture.getHeightMap());

			minHeight = std::min(minHeight, sample);
			maxHeight = std::max(maxHeight, sample);
			vert.position.y = heightFnc(sample);

			vert.texture.x = x;
			vert.texture.y = y;

			if ((i != m_size - 1) && (j != m_size - 1))
			{
				int rowIndex = i * (m_size - 1) * 2;	//each row has 2*(m_size-1) triangles
				int vertIndex = i * m_size;
				int vert2Index = (i + 1) * m_size;
				m_faces[rowIndex + j * 2] = {vertIndex + j + 1, vertIndex + j, vert2Index + j};
				m_faces[rowIndex + j * 2 + 1] = {vert2Index + j, vert2Index + j + 1, vertIndex + j + 1};
			}
		}
	}

	m_heightShader.bind();
	m_heightShader.setUniform1f("u_minHeight", heightFnc(minHeight));
	m_heightShader.setUniform1f("u_maxHeight", heightFnc(maxHeight));
	m_textureShader.bind();
	m_textureShader.setUniform1f("u_minHeight", heightFnc(minHeight));
	m_textureShader.setUniform1f("u_maxHeight", heightFnc(maxHeight));


	if (m_IB) delete m_IB;
	m_IB = new IndexBuffer(&(m_faces[0].x), (unsigned int)(m_faces.size() * 3));

	calculateNormals();

	if (m_VB) delete m_VB;
	m_VB = new VertexBuffer((void*)(&(m_vertices[0].position.x)), (unsigned int)(m_vertices.size() * sizeof(Vertex)));

	m_VA.addBuffer(*m_VB, m_layout);
}

Terrain::~Terrain()
{
	if (m_IB)
		delete m_IB;
	if (m_VB)
		delete m_VB;
}

void Terrain::adjustHeight()
{
	float minHeight = std::numeric_limits<float>::max();	//height values used for normalisation
	float maxHeight = std::numeric_limits<float>::min();
	float increment = 1.0f / m_size;
	for (int i = 0; i < m_size; i++)
	{
		for (int j = 0; j < m_size; j++)
		{
			Vertex& vert = m_vertices[i * m_size + j];

			float x = increment * j;
			float y = increment * i;
			float sample = util::sampleBitmap(noiseTexture.getWidth(), noiseTexture.getHeight(), x, y, noiseTexture.getHeightMap());
			minHeight = std::min(minHeight, sample);
			maxHeight = std::max(maxHeight, sample);
			vert.position.y = heightFnc(sample);
		}
	}
	calculateNormals();

	delete m_VB;
	m_VB = new VertexBuffer((void*)(&(m_vertices[0].position.x)), (unsigned int)(m_vertices.size() * sizeof(Vertex)));

	m_VA.addBuffer(*m_VB, m_layout);

	m_heightShader.bind();
	m_heightShader.setUniform1f("u_minHeight", heightFnc(minHeight));
	m_heightShader.setUniform1f("u_maxHeight", heightFnc(maxHeight));
	m_textureShader.bind();
	m_textureShader.setUniform1f("u_minHeight", heightFnc(minHeight));
	m_textureShader.setUniform1f("u_maxHeight", heightFnc(maxHeight));
}

void Terrain::calculateNormals()
{
	std::vector<int> cnt(m_size * m_size);

	for (int i = 0; i < m_size * m_size; i++)
	{
		m_vertices[i].normal.x = 0;
		m_vertices[i].normal.y = 0;
		m_vertices[i].normal.z = 0;
	}

	for (int i = 0; i < m_faces.size(); i++)
	{
		unsigned int& vertA = m_faces[i].x;
		unsigned int& vertB = m_faces[i].y;
		unsigned int& vertC = m_faces[i].z;
		Vertex& A = m_vertices[vertA];
		Vertex& B = m_vertices[vertB];
		Vertex& C = m_vertices[vertC];
		glm::vec3 AB = B.position - A.position;
		glm::vec3 AC = C.position - A.position;

		cnt[vertA]++;
		cnt[vertB]++;
		cnt[vertC]++;
		A.normal += glm::cross(AB, AC);
		B.normal += glm::cross(AB, AC);
		C.normal += glm::cross(AB, AC);
	}

	for (int i = 0; i < m_size * m_size; i++)
	{
		m_vertices[i].normal /= cnt[i];
		m_vertices[i].normal = glm::normalize(m_vertices[i].normal);
	}
}

float Terrain::heightFnc(float v)
{
	return m_curve.getY(v) * m_heightMultiplier;
}

void Terrain::changeSize(int size)
{
	m_size = size;
	noiseTexture.setSize(size, size);
	noiseTexture.generate(true, false);
	falloffTexture.setSize(size, size);
	falloffTexture.generate(false);
	TextureMath::calc2(noiseTexture, falloffTexture, TextureMath::Function::sub);
	m_generateMesh();
}


bool Terrain::onImGuiRender()
{
	if (ImGui::BeginTabBar("bar_settings"))
	{
		if (ImGui::BeginTabItem("Terrain"))
		{
			ImGui::Checkbox("Auto update", &m_autoUpdate);

			ImGui::Checkbox("Use falloff map", &m_useFalloff);
			if (ImGui::IsItemDeactivatedAfterEdit())
			{
				noiseTexture.generate();
			}


			if (ImGui::Button("Apply second noise map"))
			{
				noiseTexture.generate(true, false);

				if (m_modFunction == 0)
					TextureMath::calc2(noiseTexture, modTexture, TextureMath::Function::mult);
				else if (m_modFunction == 1)
					TextureMath::calc2(noiseTexture, modTexture, TextureMath::Function::sub);
				else
					TextureMath::calc2(noiseTexture, modTexture, TextureMath::Function::add);

				TextureMath::calc2(noiseTexture, falloffTexture, TextureMath::Function::sub);

				noiseTexture.generate(false, true);
			}


			ImGui::SliderFloat("Height multiplier", &m_heightMultiplier, 0, (float)m_size * 2, "%.2f");
			if (ImGui::IsItemDeactivatedAfterEdit() && m_autoUpdate)
			{
				adjustHeight();
			}

			if (ImGui::Button("Generate terrain"))
				adjustHeight();

			ImGui::NewLine();

			if (ImGui::CollapsingHeader("Curve"))
			{
				if (m_curve.onImGuiRender())
				{
					adjustHeight();
				}
			}

			ImGui::EndTabItem();
		}

		if (m_falloffWindow)
		{
			ImGui::Begin("Falloff texture");

			ImGui::Checkbox("Pop out", &m_falloffWindow);
			falloffTexture.onImGuiRender();

			ImGui::End();
		}
		else if (ImGui::BeginTabItem("Falloff texture"))
		{
			ImGui::Checkbox("Pop out", &m_falloffWindow);
			falloffTexture.onImGuiRender();

			ImGui::EndTabItem();
		}

		if (m_noiseWindow)
		{
			ImGui::Begin("Noise texture");

			ImGui::Checkbox("Pop out", &m_noiseWindow);
			ImGui::Checkbox("Show second noise map", &m_showModTexture);
			noiseTexture.onImGuiRender();

			ImGui::End();

			if (m_showModTexture)
			{
				ImGui::Begin("Mod texture");
				ImGui::RadioButton("Mult", &m_modFunction, 0);
				ImGui::RadioButton("Sub", &m_modFunction, 1);
				ImGui::RadioButton("Add", &m_modFunction, 2);

				modTexture.onImGuiRender();

				ImGui::End();
			}
		}
		else if (ImGui::BeginTabItem("Noise texture"))
		{
			ImGui::Checkbox("Pop out", &m_noiseWindow);
			ImGui::Checkbox("Show second noise map", &m_showModTexture);
			noiseTexture.onImGuiRender();

			if (m_showModTexture)
			{
				ImGui::Begin("Mod texture");
				ImGui::RadioButton("Mult", &m_modFunction, 0);
				ImGui::RadioButton("Sub", &m_modFunction, 1);
				ImGui::RadioButton("Add", &m_modFunction, 2);

				modTexture.onImGuiRender();

				ImGui::End();
			}

			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}

	return false;
}

