#pragma once
#include "openGLApp.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"
#include "glm/gtx/polar_coordinates.hpp"

#include <functional>
#include "renderer.h"
#include "texture.h"
#include "texture3d.h"
#include "gllog.h"
#include "camera.h"

#include "utility/globals.h"
#include "noiseTexture.h"
#include "skybox.h"
#include "terrain.h"
#include "utility/colorRamp.h"

class Procedural
	: public OpenGLApp
{
private:

	GLFWwindow* m_window = nullptr;
	Camera m_cam;
	
	glm::mat4 m_projMat = glm::perspective(glm::radians(90.0f), 1.0f * c_width / c_height, 0.1f, 1000.0f);	//quasi global projection matrix


	bool m_mouseLocked = true;

	void framebufferResizeCallback(GLFWwindow* window, int width, int height);
	void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void mouseCallback(GLFWwindow* window, double xpos, double ypos);
	void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

	float m_camSpeed = 1.0f;
	float m_camSpeedMod = 2.0f;
	float m_sensitivity = 0.1f;
	float m_lastX = 1.0f * c_width / 2.0f;
	float m_lastY = 1.0f * c_height / 2.0f;

	SkyBox* m_skybox = nullptr;

	Terrain m_terrain;
	
	Quad m_water;
	Shader m_waterShader;
	Texture m_waterTexture;
	Texture m_waterNormal;

	std::vector<std::string> m_texturePaths = {
		"./res/tex/water.png",
		"./res/tex/sand.jpg",
		"./res/tex/ground_1.jpg",
		"./res/tex/ground_2.jpg",
		"./res/tex/grass.png",
		"./res/tex/rocks_1.png",
		"./res/tex/rocks_2.png",
		"./res/tex/snow.jpg"
	};
	Texture3D m_textures;

	void m_setSun();
	void m_setTextures();


	glm::vec4 m_camAnimPos = glm::vec4(-260, 160, 0, 0);
	float m_camAnimAngle = 0.0f;
	float m_camAnimSpeed = 0.5f;
public:
	Procedural();
	~Procedural();

	void setCallbacks(GLFWwindow* window) override;
	void appInit() override;
	void onRender() override;
	void onImGuiRender() override;
	void inputHandle(GLFWwindow* window) override;


//ImGui variables
private:
	bool m_showHelp = true;
	bool m_showDebug = false;

	bool m_showWireframe = false;
	bool m_showHeightmap = false;
	bool m_showColors = false;
	bool m_showSkybox = true;

	bool m_colorWindow = false;
	bool m_textureWindow = false;

	int m_size = 256;

	float m_sunAngle = 60.0f;
	bool m_sunAnim = false;
	float m_sunAnimCoeff = 0.1f;

	bool m_animateCamera = false;

	float m_waterLevel = 5.0f;

	bool m_animateWater = true;
	glm::vec2 m_waterOffset = {0.0f,0.0f};
	bool m_waterDir = false;

};