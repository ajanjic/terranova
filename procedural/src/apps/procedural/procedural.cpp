#include "procedural.h"

//INPUT AND CALLBACKS
void Procedural::setCallbacks(GLFWwindow* window)
{
	//https://stackoverflow.com/questions/7676971
	glfwSetWindowUserPointer(window, this);
	m_window = window;

	auto framebuffcallback = [](GLFWwindow* window, int width, int height)
	{
		static_cast<Procedural*>(glfwGetWindowUserPointer(window))->framebufferResizeCallback(window, width, height);
	};
	auto keycallback = [](GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		static_cast<Procedural*>(glfwGetWindowUserPointer(window))->keyCallback(window, key, scancode, action, mods);
	};
	auto mousecallback = [](GLFWwindow* window, double xpos, double ypos)
	{
		static_cast<Procedural*>(glfwGetWindowUserPointer(window))->mouseCallback(window, xpos, ypos);
	};
	auto scrollcallback = [](GLFWwindow* window, double xoffset, double yoffset)
	{
		static_cast<Procedural*>(glfwGetWindowUserPointer(window))->scrollCallback(window, xoffset, yoffset);
	};


	glfwSetFramebufferSizeCallback(window, framebuffcallback);
	glfwSetKeyCallback(window, keycallback);
	glfwSetCursorPosCallback(window, mousecallback);
	glfwSetScrollCallback(window, scrollcallback);
}

void Procedural::framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
	if (width == 0 || height == 0) return;
	c_width = width;
	c_height = height;
	m_projMat = glm::perspective(glm::radians(90.0f), 1.0f * c_width / c_height, 0.3f, 1000.0f);
	glViewport(0, 0, width, height);

	m_terrain.m_heightShader.bind();
	m_terrain.m_heightShader.setUniformMat4f("u_projMat", m_projMat);

	m_terrain.m_textureShader.bind();
	m_terrain.m_textureShader.setUniformMat4f("u_projMat", m_projMat);

	m_skybox->skyShader.bind();
	m_skybox->skyShader.setUniformMat4f("u_projMat", m_projMat);

	m_skybox->cloudShader.bind();
	m_skybox->cloudShader.setUniformMat4f("u_projMat", m_projMat);

	m_waterShader.bind();
	m_waterShader.setUniformMat4f("u_projMat", m_projMat);
}

void Procedural::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_LEFT_ALT && action == GLFW_PRESS)
	{
		m_mouseLocked ^= 1;
		if (m_mouseLocked)
		{
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
		else
		{
			double xpos, ypos;
			glfwGetCursorPos(window, &xpos, &ypos);
			m_lastX = (float)xpos;
			m_lastY = (float)ypos;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
	}
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		m_camSpeed = 1.0f;
	}
	if (key == GLFW_KEY_H && action == GLFW_PRESS)
	{
		m_showHelp ^= 1;
	}
	if (key == GLFW_KEY_J && action == GLFW_PRESS)
	{
		m_showDebug ^= 1;
	}
}

void Procedural::mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (m_mouseLocked)
	{
		return;
	}

	float xoffset = (float)xpos - m_lastX;
	float yoffset = m_lastY - (float)ypos;		//y is flipped
	m_lastX = (float)xpos;
	m_lastY = (float)ypos;

	xoffset *= m_sensitivity;
	yoffset *= m_sensitivity;

	m_cam.yaw += xoffset;
	m_cam.pitch += yoffset;

	//clamp pitch so screen doesn't get flipped
	if (m_cam.pitch > 89.9f)
	{
		m_cam.pitch = 89.9f;
	}
	else if (m_cam.pitch < -89.9f)
	{
		m_cam.pitch = -89.9f;
	}

	m_cam.calcMat();
}

void Procedural::scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	if (yoffset > 0)
	{
		m_camSpeed -= m_camSpeedMod * (float)yoffset;
		if (m_camSpeed < 0)
		{
			m_camSpeed = 0.001f;
		}
	}
	else if (yoffset < 0)
	{
		m_camSpeed -= m_camSpeedMod * (float)yoffset;
	}
}

void Procedural::inputHandle(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		m_cam.moveTo(m_cam.getPos() + m_cam.getFront() * m_camSpeed * 0.07f);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		m_cam.moveTo(m_cam.getPos() - m_cam.getFront() * m_camSpeed * 0.07f);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		m_cam.moveTo(m_cam.getPos() - m_cam.getRight() * m_camSpeed * 0.07f);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		m_cam.moveTo(m_cam.getPos() + m_cam.getRight() * m_camSpeed * 0.07f);
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		m_cam.moveTo(m_cam.getPos() + m_cam.getUp() * m_camSpeed * 0.04f);
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		m_cam.moveTo(m_cam.getPos() - m_cam.getUp() * m_camSpeed * 0.04f);
	}
}

Procedural::Procedural()
	: m_terrain(256),
	m_waterShader("./res/shader/water.vert.shader", "./res/shader/water.frag.shader"), m_waterTexture("./res/tex/water.png", false), m_waterNormal("./res/tex/normal_wavea.png", false),
	m_textures(m_texturePaths, {
		{0.00f, 0.05f,"Water"},
		{0.06f, 0.05f,"Sand"},
		{0.10f, 0.05f,"Sandy ground"},
		{0.16f, 0.05f,"Ground"},
		{0.29f, 0.05f,"Grass"},
		{0.44f, 0.10f,"Rocks 1"},
		{0.70f, 0.12f,"Rocks 2"},
		{0.93f, 0.05f,"Snow"},})
{
}

Procedural::~Procedural()
{
	if (m_skybox) delete m_skybox;
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	glfwSetFramebufferSizeCallback(m_window, NULL);
	glfwSetKeyCallback(m_window, NULL);
	glfwSetCursorPosCallback(m_window, NULL);
	glfwSetScrollCallback(m_window, NULL);
}

void Procedural::appInit()
{

	m_cam.moveTo({-80,45,0});
	m_cam.yaw = 0;
	m_cam.pitch = -20;
	m_cam.calcMat();

	m_terrain.m_heightShader.bind();
	m_terrain.m_heightShader.setUniformMat4f("u_projMat", m_projMat);

	m_terrain.m_textureShader.bind();
	m_terrain.m_textureShader.setUniformMat4f("u_projMat", m_projMat);
	m_setTextures();
	m_terrain.m_textureShader.setUniform1i("u_texture", 0);
	m_terrain.m_textureShader.setUniform1i("u_skymap", 1);

	m_skybox = new SkyBox(m_cam.getViewMatPtr(), &m_projMat);


	m_waterShader.bind();
	m_waterShader.setUniformMat4f("u_projMat", m_projMat);
	m_waterShader.setUniformMat4f("u_rotMat", glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
	m_waterShader.setUniformMat4f("u_centerMat", glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, m_waterLevel, -0.5f)));
	m_waterShader.setUniformMat4f("u_scaleMat", glm::scale(glm::mat4(1.0f), glm::vec3(512.0f, 1.0f, 512.0f)));
	m_waterShader.setUniform1i("u_texture", 0);
	m_waterShader.setUniform1i("u_skymap", 1);
	m_waterShader.setUniform1i("u_normalmap", 2);

	m_setSun();

}

void Procedural::m_setSun()
{
	float angle = glm::radians(m_sunAngle);
	glm::vec3 m_sunPosition = glm::euclidean(glm::vec2(angle, glm::pi<float>() / 8)) * 1000.0f;	//azimuth is fixed to pi/8

	m_skybox->skyShader.bind();
	m_skybox->skyShader.setUniform1f("u_sunAngle", m_sunAngle);
	m_skybox->skyShader.setUniform3f("u_sunVec", m_sunPosition);
	m_skybox->cloudShader.bind();
	m_skybox->cloudShader.setUniform1f("u_sunAngle", m_sunAngle);

	m_terrain.m_textureShader.bind();
	m_terrain.m_textureShader.setUniform3f("u_sunPosition", m_sunPosition);
	m_terrain.m_textureShader.setUniform1f("u_sunAngle", m_sunAngle);

	m_waterShader.bind();
	m_waterShader.setUniform3f("u_sunPos", m_sunPosition);
	m_waterShader.setUniform1f("u_sunAngle", m_sunAngle);
}

void Procedural::m_setTextures()
{
	m_terrain.m_textureShader.bind();
	m_terrain.m_textureShader.setUniform1i("u_texNum", (int)m_textures.properties.size());
	m_terrain.m_textureShader.setUniform1f("u_scale", m_textures.getScale());

	for (int i = 0; i < m_textures.properties.size(); i++)
	{
		char buffer[50];
		sprintf_s(buffer, 50, "u_texValues[%d]", i);
		m_terrain.m_textureShader.setUniform1f(buffer, m_textures.properties[i].value);
		sprintf_s(buffer, 50, "u_blendAmounts[%d]", i);
		m_terrain.m_textureShader.setUniform1f(buffer, m_textures.properties[i].blendAmount);
	}
}

void Procedural::onRender()
{
	Renderer::clear();
	if (m_skybox && m_showSkybox)
		m_skybox->render();

	if (m_animateCamera)
	{
		glm::vec3 newCoords = glm::rotate(glm::mat4(1), m_camAnimAngle, glm::vec3(0, 1, 0)) * m_camAnimPos;
		m_cam.moveTo(newCoords);
		m_camAnimAngle += 0.0001f + m_camAnimSpeed / 50;
		if (m_camAnimAngle > glm::pi<float>() * 2)
		{
			m_camAnimAngle = 0.0f;
		}
		m_cam.lookAt({0,0,0});
	}

	if (m_sunAnim)
	{
		m_sunAngle += m_sunAnimCoeff;
		if (m_sunAngle > 180)	//night passes faster
		{
			m_sunAnimCoeff = 0.2f;
		}
		else
		{
			m_sunAnimCoeff = 0.1f;
		}
		if (m_sunAngle >= 360.0f)
		{
			m_sunAngle = 0.0f;
		}

		m_setSun();
	}

	if (m_showWireframe || m_showHeightmap)
	{
		m_terrain.m_heightShader.bind();
		m_terrain.m_heightShader.setUniformMat4f("u_viewMat", m_cam.getViewMat());
		if (m_showWireframe)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		Renderer::draw(m_terrain.getVA(), m_terrain.getIB(), m_terrain.m_heightShader);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		m_textures.bind();
		m_skybox->skymap.bind(1);
		m_terrain.m_textureShader.bind();
		m_terrain.m_textureShader.setUniformMat4f("u_viewMat", m_cam.getViewMat());

		Renderer::draw(m_terrain.getVA(), m_terrain.getIB(), m_terrain.m_textureShader);

		m_waterTexture.bind();
		m_skybox->skymap.bind(1);
		m_waterNormal.bind(2);
		m_waterShader.bind();
		m_waterShader.setUniformMat4f("u_viewMat", m_cam.getViewMat());
		m_waterShader.setUniform3f("u_camPos", m_cam.getPos());
		m_waterShader.setUniform2f("u_offset", m_waterOffset);

		m_waterOffset += glm::vec2(0.00005, 0.0002);
		if (m_waterOffset.x > 1.0f)
		{
			m_waterOffset.x = 0.0f;
			m_waterOffset.y = 0.0f;
		}

		Renderer::draw(m_water.getVA(), m_water.getIB(), m_waterShader);
	}
}

void Procedural::onImGuiRender()
{

	if (m_showHelp)	//toggle with H
	{
		ImGui::Begin("Help");

		ImGui::LabelText("##", "Movement: 'WASD', 'Space', 'Shift'");
		ImGui::LabelText("##", "'LAlt' to lock/unlock mouse movement");
		ImGui::LabelText("##", "Mwheel down to increase camera speed");
		ImGui::LabelText("##", "Mwheel up to decrease camera speed");
		ImGui::LabelText("##", "'R' to reset camera speed");
		ImGui::LabelText("##", "Toggle this window with 'H'");

		ImGui::End();
	}

	if (m_showDebug)	//toggle with J
	{
		ImGui::Begin("Debug");

		ImGui::LabelText("Camera position", "%f, %f, %f", m_cam.getPos()[0], m_cam.getPos()[1], m_cam.getPos()[2]);
		ImGui::LabelText("Camera dir", "%f, %f, %f", m_cam.getDir()[0], m_cam.getDir()[1], m_cam.getDir()[2]);

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

		ImGui::End();
	}

	{
		ImGui::Begin("Settings");
		m_terrain.onImGuiRender();

		if (ImGui::BeginTabBar("bar_settings"))
		{
			if (m_textureWindow)
			{
				ImGui::Begin("Texture");

				ImGui::Checkbox("Pop out", &m_textureWindow);
				if (m_textures.onImGuiRender())
				{
					m_setTextures();
				}

				ImGui::End();
			}
			else if (ImGui::BeginTabItem("Texture"))
			{
				ImGui::Checkbox("Pop out", &m_textureWindow);
				if (m_textures.onImGuiRender())
				{
					m_setTextures();
				}

				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("Animation"))
			{
				if (ImGui::SliderFloat("Sun angle", &m_sunAngle, 0.0f, 360.0f, "%.1f deg"))
				{
					m_setSun();
				}

				ImGui::Checkbox("Animate sun", &m_sunAnim);

				ImGui::Checkbox("Animate camera", &m_animateCamera);

				ImGui::SliderFloat("Camera animation speed", &m_camAnimSpeed, 0.0f, 1.0f, "%.2f");

				if (ImGui::Button("Lookat"))
				{
					m_cam.lookAt(glm::vec3(0, 0, 0));
				}

				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("Other"))
			{
				ImGui::Checkbox("Display wireframe", &m_showWireframe);

				ImGui::Checkbox("Display heightmap", &m_showHeightmap);

				if (ImGui::Checkbox("Display colors only", &m_showColors))
				{
					m_terrain.m_textureShader.bind();
					m_terrain.m_textureShader.setUniform1i("u_colorsOnly", m_showColors);
				}

				ImGui::Checkbox("Display skybox", &m_showSkybox);

				ImGui::SliderInt("Plane size", &m_size, 32, 1024, "%d");
				if (ImGui::IsItemDeactivatedAfterEdit())
				{
					m_terrain.changeSize(m_size);
					m_waterShader.bind();
					m_waterShader.setUniformMat4f("u_scaleMat", glm::scale(glm::mat4(1.0f), glm::vec3(m_size * 2, 1.0f, m_size * 2)));
				}



				ImGui::SliderFloat("Water level", &m_waterLevel, 0.0f, 100.0f, "%.2f");
				if (ImGui::IsItemDeactivatedAfterEdit())
				{
					m_waterShader.bind();
					m_waterShader.setUniformMat4f("u_centerMat", glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, m_waterLevel, -0.5f)));
				}

				ImGui::EndTabItem();
			}

			ImGui::EndTabBar();
		}

		ImGui::End();
	}
}

