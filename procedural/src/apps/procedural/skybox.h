#pragma once
#include <array>
#include <string>
#include "shader.h"
#include "vertexarray.h"
#include "indexbuffer.h"
#include "renderer.h"
#include "stb/stb_image.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "texture.h"
#include "utility/textureMath.h"
#include "noisetexture.h"
#include "falloffTexture.h"
#include "object.h"

class SkyBox
{
private:
	NoiseTexture m_cloudmap;
	NoiseTexture m_cloudmapMod;    //rougher more blotchy texture used to break up the main cloud tex
	FalloffTexture m_cloudFalloff;

	IndexBuffer* m_ibo = nullptr;
	VertexBufferLayout* layout = nullptr;
	VertexBuffer* m_vb = nullptr;
	VertexArray m_vao;

	Quad m_cloudLayer;

	const glm::mat4* m_viewRef;
	const glm::mat4* m_projRef;
public:
	SkyBox(const glm::mat4* viewMat, const glm::mat4* projMat);
	~SkyBox();

	glm::mat4 modelMat = glm::scale(glm::mat4(1.0f), glm::vec3(20.0f, 1.0f, 20.0f)) * glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, 1.0f, -0.5f)) * glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

	Texture skymap;	//gradient map: x is time y is height
	Shader skyShader;
	Shader cloudShader;    

	void render();
};