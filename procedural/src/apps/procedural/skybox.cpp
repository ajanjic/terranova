#include "skybox.h"
#include "glm/gtc/matrix_transform.hpp"
SkyBox::SkyBox(const glm::mat4* viewMat, const glm::mat4* projMat)
	: skyShader("./res/shader/skybox.shader"), cloudShader("./res/shader/clouds.shader"), skymap("./res/tex/skymap.png"), 
	m_cloudmap(512, 512, 0, 25.0f, 0.388f, 2.05f, 5), 
	m_cloudmapMod(512, 512, 0, 85.0f, 0.2f, 0.8f, 2, 1.6f), 
	m_cloudFalloff(512,512, 1.25f, 3.0f),
	m_viewRef(viewMat), m_projRef(projMat), m_cloudLayer()
{
	m_cloudmap.generate();
	m_cloudmapMod.generate();

	TextureMath::calc2(m_cloudmap, m_cloudmapMod, TextureMath::Function::sub);
	TextureMath::calc2(m_cloudmap, m_cloudFalloff, TextureMath::Function::sub);
	m_cloudmap.generate(false);


	float vertexPos[] =
	{
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f
	};

	unsigned int indicies[] =
	{
		0,1,2,
		1,3,2,
		4,5,6,
		6,5,7,
		2,4,6,
		2,0,4,
		3,5,1,
		3,7,5,
		0,1,4,
		5,4,1,
		3,6,7,
		2,6,3
	};
	m_ibo = new IndexBuffer(indicies, 3 * 12);
	m_vb = new VertexBuffer(vertexPos, 8 * 3 * sizeof(float));
	layout = new VertexBufferLayout();
	layout->push<float>(3);

	m_vao.addBuffer(*m_vb, *layout);

	skyShader.bind();
	skyShader.setUniformMat4f("u_projMat", *m_projRef);
	skyShader.setUniform1i("u_skymap", 0);

	cloudShader.bind();
	cloudShader.setUniformMat4f("u_projMat", *m_projRef);
	cloudShader.setUniform1i("u_cloudmap", 0);

}

SkyBox::~SkyBox()
{
	delete layout;
	delete m_vb;
	delete m_ibo;
}

void SkyBox::render()
{
	glDepthMask(GL_FALSE);
	skymap.bind();
	
	skyShader.bind();
	skyShader.setUniformMat4f("u_viewMat", *m_viewRef);
	Renderer::draw(m_vao, *m_ibo, skyShader);

	m_cloudmap.getTexture()->bind();
	cloudShader.bind();
	cloudShader.setUniformMat4f("u_viewMat", *m_viewRef);
	cloudShader.setUniformMat4f("u_modelMat", modelMat);
	Renderer::draw(m_cloudLayer.getVA(), m_cloudLayer.getIB(), cloudShader);

	glDepthMask(GL_TRUE);
}