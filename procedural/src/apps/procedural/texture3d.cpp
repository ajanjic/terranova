#include "texture3D.h"


Texture3D::Texture3D(const std::vector<std::string>& filepaths, const std::vector<Texture3DElement>& props)
	:m_rendererID(0), m_width(512), m_height(512), m_layerNum(filepaths.size()), properties(props)
{
	if (props.size() != m_layerNum)
	{
		properties.resize(m_layerNum);
		for (int i = 0; i < m_layerNum; i++)
		{
			properties[i].value = (float)i / m_layerNum;
			properties[i].name = "";
			properties[i].blendAmount = 0.05f;
		}
	}

	stbi_set_flip_vertically_on_load(1);

	glGenTextures(1, &m_rendererID);
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_rendererID);

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, m_width, m_height, m_layerNum, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	for (int i = 0; i < m_layerNum; i++)
	{
		int bpp;
		unsigned char* m_databuffer = stbi_load(filepaths[i].c_str(), &m_width, &m_height, &bpp, 4);
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, m_width, m_height, 1, GL_RGBA, GL_UNSIGNED_BYTE, m_databuffer);
		if (m_databuffer)
		{
			stbi_image_free(m_databuffer);
		}
	}

	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

Texture3D::~Texture3D()
{
	glDeleteTextures(1, &m_rendererID);
}


void Texture3D::bind(unsigned int slot) const
{
	ASSERT_MSG(slot < 32, "Binding Texture3D to non existent slot.");
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_rendererID);
}

void Texture3D::unbind() const
{
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

bool Texture3D::onImGuiRender()
{
	bool ret = false;
	for (int i = 0; i < m_layerNum; i++)
	{
		ImGui::PushID(i);

		ImGui::LabelText("", properties[i].name.c_str());
		if (ImGui::DragFloat("Height", &properties[i].value, 0.01f, 0.001f, 1.0f, "%.2f"))
		{
			ret = true;
		}
		if (ImGui::DragFloat("Blend", &properties[i].blendAmount, 0.01f, 0.0f, 1.0f, "%.2f"))
		{
			ret = true;
		}


		ImGui::PopID();
		ImGui::NewLine();
	}

	if (ImGui::SliderFloat("Texture scale", &m_scale, 0.0f, 100.0f, "%.2f"))
	{
		ret = true;
	}
	return ret;
}