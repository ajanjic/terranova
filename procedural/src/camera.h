#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera
{
private:
	glm::vec3 m_pos;
	glm::vec3 m_dir;
	glm::vec3 m_up;
	glm::mat4 m_viewMat;


public:
	float yaw = -90.0f;
	float pitch = 0.0f;

	Camera(const glm::vec3& pos, const glm::vec3& dir, const glm::vec3& up);
	Camera(const glm::vec3& pos);
	Camera();

	void lookAt(const glm::vec3& pos);
	void moveTo(const glm::vec3& pos);

	const glm::mat4& getViewMat() const;
	glm::mat4* getViewMatPtr();
	glm::vec3 getPos() const;
	glm::vec3 getDir() const;

	glm::vec3 getUp() const;
	glm::vec3 getFront() const;
	glm::vec3 getRight() const;

	void calcMat();
};