#pragma once
#include "utility/util.h"
#include <vector>
#include <GL/glew.h>
#include "primitives.h"

struct VertexElement
{
	unsigned int type;	//GLType
	unsigned int count;
	unsigned char normalized;

	static unsigned int getSizeof(unsigned int type)
	{
		switch (type)
		{
			case GL_FLOAT:  return 4;
			case GL_DOUBLE:  return 8;
			case GL_UNSIGNED_INT: return 4;
			case GL_UNSIGNED_BYTE: return 1;
		}
		ASSERT_MSG(false, "Unknown type.");
		return 0;
	}
};

class VertexBufferLayout
{
private:
	std::vector<VertexElement> m_elements;
	unsigned int m_stride;

public:
	VertexBufferLayout(const std::vector<VertexElement>& elements);
	VertexBufferLayout();

	void clear();

	template<typename T> void push(unsigned int count, unsigned char normalized = 0);
	template<> void push<float>(unsigned int count, unsigned char normalized);
	template<> void push<double>(unsigned int count, unsigned char normalized);
	template<> void push<unsigned int>(unsigned int count, unsigned char normalized);
	template<> void push<unsigned char>(unsigned int count, unsigned char normalized);
	template<> void push<Vertex>(unsigned int count, unsigned char normalized);

	inline const std::vector<VertexElement>& getElements() const { return m_elements; }
	inline unsigned int getStride() const { return m_stride; }
};