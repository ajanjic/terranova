#include "renderer.h"


void Renderer::draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader)
{
	shader.bind();
	va.bind();
	ib.bind();

	glDrawElements(GL_TRIANGLES, ib.getCount(), GL_UNSIGNED_INT, nullptr);

	DEBUGCALL(
		va.unbind();
		shader.unbind();
		ib.unbind();
	)
}

void Renderer::drawLines(const VertexArray& va, const IndexBuffer& ib, const Shader& shader)
{
	shader.bind();
	va.bind();
	ib.bind();

	glDrawElements(GL_LINES, ib.getCount(), GL_UNSIGNED_INT, nullptr);

	DEBUGCALL(
		va.unbind();
		shader.unbind();
		ib.unbind();
	)
}

void Renderer::clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::setClearColor(const glm::vec4& color)
{
	glClearColor(color[0], color[1], color[2], color[3]);
}
