#include "vertexarray.h"
#include "renderer.h"

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &m_rendererID);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &m_rendererID);
}

void VertexArray::addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout)
{
	bind();
	vb.bind();

	const auto& elements = layout.getElements();
	unsigned int offsetPtr = 0;
	for (unsigned int i = 0; i < elements.size(); i++)
	{
		const auto& elem = elements[i];
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, elem.count, elem.type,
			elem.normalized, layout.getStride(), (const void*)offsetPtr);

		offsetPtr += elem.count * VertexElement::getSizeof(elem.type);
	}
}

void VertexArray::bind() const
{
	glBindVertexArray(m_rendererID);
}


void VertexArray::unbind() const
{
	glBindVertexArray(0);
}
