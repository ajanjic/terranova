#include "shader.h"


ShaderProgramSource Shader::parseShader(const std::string& filepath)
{
	std::ifstream stream(filepath);
	ASSERT_MSG(stream.is_open(), std::string("ERROR opening file") + filepath);

	enum class ShaderType
	{
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};

	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while (getline(stream, line))
	{
		if (line.find("#shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
			{
				type = ShaderType::VERTEX;
			}
			else if (line.find("fragment") != std::string::npos)
			{
				type = ShaderType::FRAGMENT;
			}
		}
		else
		{
			ss[(int)type] << line << '\n';
		}
	}

	return {ss[0].str(), ss[1].str()};
}

unsigned int Shader::compileShader(unsigned int type, const std::string& source)
{
	unsigned int id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::cout << "Failed to compile" << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << "shader" << std::endl;
		std::cout << message << std::endl;
		glDeleteShader(id);

		return 0;
	}

	return id;
}

unsigned int Shader::createShader(const std::string& vertShader, const std::string& fragShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = compileShader(GL_VERTEX_SHADER, vertShader);
	unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragShader);

	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}

int Shader::getUniformLocation(const std::string& name) const
{
	if (m_uniformCache.find(name) != m_uniformCache.end())
	{
		return m_uniformCache[name];
	}
	int location = glGetUniformLocation(m_rendererID, name.c_str());

	if (location == -1)
	{
		std::cout << "Warning: uniform '" << name << "' doesn't exist." << std::endl;
	}

	m_uniformCache[name] = location;

	return location;
}

Shader::Shader(const std::string& filename)
	: m_filePath{filename, filename}
{
	setShader(filename);
}

Shader::Shader(const std::string& vertexFilename, const std::string& fragmentFilename)
	: m_filePath{vertexFilename, fragmentFilename}
{
	setShader(vertexFilename, fragmentFilename);
}

Shader::~Shader()
{
	glDeleteProgram(m_rendererID);
}



void Shader::bind() const
{
	glUseProgram(m_rendererID);
}

void Shader::unbind() const
{
	glUseProgram(0);
}

void Shader::setShader(const std::string& filename)
{
	m_filePath[0] = m_filePath[1] = filename;
	m_uniformCache.clear();
	ShaderProgramSource source = parseShader(filename);
	m_rendererID = createShader(source.vertexSource, source.fragmentSource);
}

void Shader::setShader(const std::string& vertexFilename, const std::string& fragmentFilename)
{
	m_filePath[0] = vertexFilename;
	m_filePath[1] = fragmentFilename;
	m_uniformCache.clear();

	std::string vertexSource = parseShader(vertexFilename).vertexSource;
	std::string fragmentSource = parseShader(fragmentFilename).fragmentSource;
	m_rendererID = createShader(vertexSource, fragmentSource);
}

void Shader::setUniform1i(const std::string& name, int v0) const
{
	glUniform1i(getUniformLocation(name), v0);
}
void Shader::setUniform1iv(const std::string& name, unsigned int count, const int* values) const
{
	glUniform1iv(getUniformLocation(name), count, values);
}


void Shader::setUniform1f(const std::string& name, float v0) const
{
	glUniform1f(getUniformLocation(name), v0);
}
void Shader::setUniform2f(const std::string& name, float v0, float v1) const
{
	glUniform2f(getUniformLocation(name), v0, v1);
}
void Shader::setUniform2f(const std::string& name, const glm::vec2& value) const
{
	glUniform2f(getUniformLocation(name), value.x, value.y);
}
void Shader::setUniform3f(const std::string& name, float v0, float v1, float v2) const
{
	glUniform3f(getUniformLocation(name), v0, v1, v2);
}
void Shader::setUniform3f(const std::string& name, const glm::vec3& value) const
{
	glUniform3f(getUniformLocation(name), value.x, value.y, value.z);
}
void Shader::setUniform4f(const std::string& name, float v0, float v1, float v2, float v3) const
{
	glUniform4f(getUniformLocation(name), v0, v1, v2, v3);
}
void Shader::setUniform4f(const std::string& name, const glm::vec4& value) const
{
	glUniform4f(getUniformLocation(name), value.x, value.y, value.z, value.w);
}


void Shader::setUniformMat3f(const std::string& name, const glm::mat3& matrix) const
{
	glUniformMatrix3fv(getUniformLocation(name), 1, GL_FALSE, &matrix[0][0]);
}
void Shader::setUniformMat4f(const std::string& name, const glm::mat4& matrix) const
{
	glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, &matrix[0][0]);
}
