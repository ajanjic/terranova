#pragma once
#include "gllog.h"
#include "stb/stb_image.h"

class Texture
{
private:
	unsigned int m_rendererID;
	int m_width, m_height, m_bpp;
	unsigned int m_format;

public:
	Texture(const std::string& filepath, bool clamp = true);
	Texture(int width, int height, int m_bpp, unsigned char* data);
	~Texture();

	void bind(unsigned int slot = 0) const;    //slot number depends on GPU
	void unbind() const;

	void updateData(unsigned char* data);

	inline int getWidth() const { return m_width; }
	inline int getHeight() const { return m_height; }
	inline int getBPP() const { return m_bpp; }
	inline unsigned int getID() const { return m_rendererID; }
};