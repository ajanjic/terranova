#include "gllog.h"


void OpenGLLog(const char* message, const char* type)
{
	std::cout << type << ": " << message << std::endl;
}

void GLAPIENTRY OpenGLErrorCallback(
	unsigned int source,
	unsigned int type,
	unsigned int id,
	unsigned int severity,
	int length,
	const char* message,
	const void* userParam)
{
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         OpenGLLog(message, "CRITICAL"); return;
	case GL_DEBUG_SEVERITY_MEDIUM:       OpenGLLog(message, "ERROR"); return;
	case GL_DEBUG_SEVERITY_LOW:          OpenGLLog(message, "WARN"); return;
	case GL_DEBUG_SEVERITY_NOTIFICATION: OpenGLLog(message, "TRACE"); return;
	}

	ASSERT_MSG(false, "Unknown severity level.");
}
