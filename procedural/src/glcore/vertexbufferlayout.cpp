#include "vertexbufferlayout.h"

VertexBufferLayout::VertexBufferLayout(const std::vector<VertexElement>& elements)
	: m_elements(elements), m_stride(0)
{
	for (auto& e : elements)
	{
		m_stride += e.count * VertexElement::getSizeof(e.type);
	}
}

VertexBufferLayout::VertexBufferLayout()
	: m_stride(0)
{
}

template<typename T>
void VertexBufferLayout::push(unsigned int count, unsigned char normalized)
{
	static_assert(false);
}

template<>
void VertexBufferLayout::push<float>(unsigned int count, unsigned char normalized)
{
	m_elements.push_back({GL_FLOAT, count, normalized});
	m_stride += count * VertexElement::getSizeof(GL_FLOAT);
}

template<>
void VertexBufferLayout::push<double>(unsigned int count, unsigned char normalized)
{
	m_elements.push_back({GL_DOUBLE, count, normalized});
	m_stride += count * VertexElement::getSizeof(GL_DOUBLE);
}

template<>
void VertexBufferLayout::push<unsigned int>(unsigned int count, unsigned char normalized)
{
	m_elements.push_back({GL_UNSIGNED_INT, count, normalized});
	m_stride += count * VertexElement::getSizeof(GL_UNSIGNED_INT);
}

template<>
void VertexBufferLayout::push<unsigned char>(unsigned int count, unsigned char normalized)
{
	m_elements.push_back({GL_UNSIGNED_BYTE, count, normalized});
	m_stride += count * VertexElement::getSizeof(GL_UNSIGNED_BYTE);
}

template<>
void VertexBufferLayout::push<Vertex>(unsigned int count, unsigned char normalized)
{
	push<float>(3);
	push<float>(3);
	push<float>(2);
}

void VertexBufferLayout::clear()
{
	m_stride = 0;
	m_elements.clear();
}