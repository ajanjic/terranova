#pragma once
#include "utility/util.h"
#include "vertexarray.h"
#include "indexbuffer.h"
#include "shader.h"
#include <iostream>

class Renderer
{
public:
	static void draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader);
	static void drawLines(const VertexArray& va, const IndexBuffer& ib, const Shader& shader);
	static void clear();

	static void setClearColor(const glm::vec4& color);
};