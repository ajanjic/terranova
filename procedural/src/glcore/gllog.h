#pragma once
#include <GL/glew.h>
#include "utility/util.h"
#include <iostream>

void OpenGLLog(const char* message, const char* type);
void GLAPIENTRY OpenGLErrorCallback(
	unsigned int source,
	unsigned int type,
	unsigned int id,
	unsigned int severity,
	int length,
	const char* message,
	const void* userParam);