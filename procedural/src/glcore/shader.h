#pragma once
#include "gllog.h"
#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include "glm/glm.hpp"


struct ShaderProgramSource
{
	std::string vertexSource;
	std::string fragmentSource;
};

class Shader
{
private:
	std::string m_filePath[2];
	unsigned int m_rendererID;
	mutable std::unordered_map<std::string, int> m_uniformCache;


	ShaderProgramSource parseShader(const std::string& filepath);
	unsigned int compileShader(unsigned int type, const std::string& source);
	unsigned int createShader(const std::string& vertShader, const std::string& fragShader);

	int getUniformLocation(const std::string& name) const;
public:

	Shader(const std::string& filename);
	Shader(const std::string& vertexFilename, const std::string& fragmentFilename);
	~Shader();

	void bind() const;	//technically gluseProgram
	void unbind() const;

	void setShader(const std::string& filepath);
	void setShader(const std::string& vertexFilename, const std::string& fragmentFilename);


	//uniforms
	void setUniform1i(const std::string& name, int v0) const;
	void setUniform1iv(const std::string& name, unsigned int count, const int* values) const;

	void setUniform1f(const std::string& name, float v0) const;
	void setUniform2f(const std::string& name, float v0, float v1) const;
	void setUniform2f(const std::string& name, const glm::vec2& value) const;
	void setUniform3f(const std::string& name, float v0, float v1, float v2) const;
	void setUniform3f(const std::string& name, const glm::vec3& value) const;
	void setUniform4f(const std::string& name, float v0, float v1, float v2, float v3) const;
	void setUniform4f(const std::string& name, const glm::vec4& value) const;
	
	void setUniformMat3f(const std::string& name, const glm::mat3& matrix) const;
	void setUniformMat4f(const std::string& name, const glm::mat4& matrix) const;
};