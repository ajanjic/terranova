#include "NoiseTexture.h"

NoiseTexture::NoiseTexture(int w, int h, int seed /*= 0*/, float scale /*= 55.0f*/, float gain /*= 0.5f*/, float lac /*= 1.67f*/, int octv /*= 3*/, float freq /*= 1.0f*/, glm::vec3 offset /*= {0,0,0}*/)
	: m_width(w), m_height(h), m_strSeed(new char[16]),
	m_noiseEngine(new FastNoiseLite()), m_noiseData(new unsigned char[w * h]), m_heightData(new float[w * h]), m_offset(offset)
{
	m_noiseEngine->SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2S);
	m_noiseEngine->SetFractalType(FastNoiseLite::FractalType::FractalType_FBm);

	setSeed(seed);
	setScale(scale);
	setOctaves(octv);
	setFrequency(freq);
	setGain(gain);
	setLacunarity(lac);
	//setOffset(offset);
}

NoiseTexture::~NoiseTexture()
{
	delete[] m_strSeed;
	delete[] m_heightData;
	delete[] m_noiseData;

	delete m_noiseEngine;
	if (m_texture)
		delete m_texture;
}

void NoiseTexture::setSize(int w, int h)
{
	delete[] m_heightData;
	delete[] m_noiseData;
	m_width = w;
	m_height = h;
	m_heightData = new float[w * h];
	m_noiseData = new unsigned char[w * h];
}

void NoiseTexture::setSeed(int seed)
{
	if (seed == 0)
	{
		std::random_device rd;
		std::default_random_engine engine(rd());
		std::uniform_int_distribution<int> distrib(1);

		m_seed = distrib(engine);
	}
	else
	{
		m_seed = seed;
	}

	m_noiseEngine->SetSeed(m_seed);
	sprintf_s(m_strSeed, 16, "%s", std::to_string(m_seed).c_str());
}

void NoiseTexture::generateHeightMap()
{
	m_maxNoiseVal = std::numeric_limits<float>::min();
	m_minNoiseVal = std::numeric_limits<float>::max();

	for (int i = 0; i < m_height; i++)
	{
		for (int j = 0; j < m_width; j++)
		{
			float sampleX = ((float)i + m_offset.x) / m_scale;
			float sampleY = ((float)j + m_offset.y) / m_scale;
			float noiseHeight = m_noiseEngine->GetNoise(sampleX, sampleY, m_offset.z);

			m_heightData[i * m_width + j] = noiseHeight;

			if (noiseHeight > m_maxNoiseVal)
				m_maxNoiseVal = noiseHeight;
			if (noiseHeight < m_minNoiseVal)
				m_minNoiseVal = noiseHeight;
		}
	}

	for (int i = 0; i < m_height; i++)
	{
		for (int j = 0; j < m_width; j++)
		{
			m_heightData[i * m_width + j] -= m_minNoiseVal;
			m_heightData[i * m_width + j] /= m_maxNoiseVal - m_minNoiseVal;
		}
	}
}

void NoiseTexture::m_calculateTexture()
{
	for (int i = 0; i < m_height; i++)
	{
		for (int j = 0; j < m_width; j++)
		{
			const float& val = m_heightData[i * m_width + j];
			m_noiseData[i * m_width + j] = (unsigned char)(val * 255);
		}
	}
}

void NoiseTexture::generate(bool recalculateNoise /*= true*/, bool callback /*= true*/)
{
	if (recalculateNoise)
		generateHeightMap();

	m_calculateTexture();

	if (m_texture)
		delete m_texture;
	m_texture = new Texture(m_width, m_height, 1, m_noiseData);

	if (m_onGenFunction && callback)
		m_onGenFunction();
}

void NoiseTexture::onImGuiRender()
{
	//ImGui::Begin("Noise parameters");

	ImGui::Image((void*)m_texture->getID(), {256, 256});

	if (ImGui::CollapsingHeader("Parameters"))
	{
		ImGui::LabelText("", "Offsets");
		if (ImGui::SliderFloat2("XY", &m_offset.x, 0.0f, 100.0f, "%.3f"))
		{
			setOffset(m_offset);
			generate();
		}
		if (ImGui::SliderFloat("Z", &m_offset.z, 0.0f, 10.0f, "%.3f"))
		{
			setOffset(m_offset);
			generate();
		}

		ImGui::NewLine();
		bool checkChange = false;
		ImGui::InputText("Seed", m_strSeed, 12, ImGuiInputTextFlags_CharsDecimal);
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			long long int aux = std::stoll(m_strSeed);
			if (aux > (1LL << 31) - 1)
			{
				m_seed = (1LL << 31) - 1;
			}
			else if (aux < (1 << 31))
			{
				m_seed = (1 << 31);
			}
			else { m_seed = (int)aux; }
			setSeed(m_seed);
			checkChange = true;
		}
		if (ImGui::Button("Random seed"))
		{
			setSeed(0);
			checkChange = true;
		}
		ImGui::SliderInt("Octaves", &m_octaves, 0, 8);
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			setOctaves(m_octaves);
			checkChange = true;
		}
		ImGui::SliderFloat("Scale", &m_scale, 0.0f, 512.0f, "%.3f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			setScale(m_scale);
			checkChange = true;
		}
		ImGui::SliderFloat("Frequency", &m_frequency, 0.0f, 2.0f, "%.3f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			setFrequency(m_frequency);
			checkChange = true;
		}
		ImGui::SliderFloat("Gain", &m_gain, 0.0f, 2.0f, "%.3f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			setGain(m_gain);
			checkChange = true;
		}
		ImGui::SliderFloat("Lacunarity", &m_lacunarity, 0.0f, 3.0f, "%.3f");
		if (ImGui::IsItemDeactivatedAfterEdit())
		{
			setLacunarity(m_lacunarity);
			checkChange = true;
		}

		//if (m_autoUpdate && checkChange)
		if (checkChange)
		{
			generate();
		}
	}
	if (ImGui::Button("Generate"))
	{
		generate();
	}

	//ImGui::End();
}

void NoiseTexture::setTextureCallback(const std::function<void()>& fnc)
{
	m_onGenFunction = fnc;
}