﻿#include <iostream>
#include <unordered_map>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"

#include "renderer.h"
#include "texture.h"
#include "gllog.h"
#include "camera.h"

#include "apps/apps.h"


//#define ENABLE_NVIDIA_GPU
#ifdef ENABLE_NVIDIA_GPU
#include "windows.h"
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}
#endif


#ifdef WINDOWED
unsigned int c_width = 960;
unsigned int c_height = 720;
#else
unsigned int c_width = 1920;
unsigned int c_height = 1080;
#endif

int main(int argc, char** argv)
{
	std::unordered_map<std::string, std::function<OpenGLApp* ()>> appMap =
	{
		{"procedural", []() {return new Procedural; }},
		{"noisetexturetest", []() {return new NoiseTextureTest; }}
	};

	GLFWwindow* window;

	if (!glfwInit())
		return -1;
	//glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

#ifdef WINDOWED
	window = glfwCreateWindow(c_width, c_height, "Terra nova", NULL, NULL);
#else
	window = glfwCreateWindow(c_width, c_height, "Terra nova", glfwGetPrimaryMonitor(), NULL);
#endif

	if (!window)
	{
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
		std::cout << "ERROR!" << std::endl;

#ifdef _DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(OpenGLErrorCallback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
#endif // _DEBUG

	std::cout << glGetString(GL_VERSION) << std::endl;

	//zbuffer
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ImGui::CreateContext();
	ImGui_ImplOpenGL3_Init();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui::StyleColorsDark();


	OpenGLApp* currentApp = nullptr;
	AppMainMenu* appMainMenu = new AppMainMenu(currentApp, window);
	//registering applications
	appMainMenu->registerApp<Procedural>("Procedural");
	appMainMenu->registerApp<NoiseTextureTest>("Noise Texture");


	//check cmd inputs for start test
	if (argc > 1)
	{
		if (appMap.find(argv[1]) != appMap.end())
		{
			currentApp = appMap[argv[1]]();
		}
	}
	else
	{
		currentApp = new Procedural;
		//currentApp = appMainMenu;
	}

	currentApp->appInit();
	currentApp->setCallbacks(window);

	while (!glfwWindowShouldClose(window))
	{
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		currentApp->onRender();
		currentApp->inputHandle(window);

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		currentApp->onImGuiRender();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);
		glfwPollEvents();

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, 1);
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS)
		{
			if (currentApp != appMainMenu)
			{
				if (currentApp)
				{
					delete currentApp;
					currentApp = nullptr;
				}
				currentApp = appMainMenu;
			}

		}
	}

	if (currentApp != appMainMenu)
	{
		delete appMainMenu;
		appMainMenu = nullptr;
	}

	if (currentApp)
	{
		delete currentApp;
		currentApp = nullptr;
	}

	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();
	return 0;
}