#pragma once
#include "glm/glm.hpp"
#include "imgui/imgui.h"

#include <numeric>
#include <random>
#include <string>
#include <functional>
#include <iostream>
#include <algorithm>

#include "texture.h"
#include "utility/util.h"

class FalloffTexture
{
private:
	int m_width;
	int m_height;

	unsigned char* m_textureData;
	float* m_data;

	Texture* m_texture = nullptr;

	//callback function on falloff generation
	std::function<void()> m_onGenFunction = nullptr;

public:
	FalloffTexture(int w, int h, float amt = 0.35f, float coeff = 2.0f);
	~FalloffTexture();

	void generate(bool callback = true);

	//callback function which gets called on every generation call
	void setTextureCallback(const std::function<void()>& fnc);
	
public: //getters/setters
	void setSize(int w, int h);
	void setAmount(float amt) { m_falloffAmt = amt; }
	void setCoefficient(float coeff) { m_coeff = coeff; }

	inline int getWidth() const { return m_width; }
	inline int getHeight() const { return m_height; }

	inline float* getData() const { return m_data; }
	inline Texture* getTexture() const { return m_texture; }

//ImGui variables and functions
private:
	//variables used for ImGui controls
	bool m_autoUpdate = false;
	float m_falloffAmt;
	float m_coeff;

	int m_widthAux;
	int m_heightAux;
public:
	void onImGuiRender();
};