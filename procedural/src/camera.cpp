#include "camera.h"


Camera::Camera(const glm::vec3& pos, const glm::vec3& dir, const glm::vec3& up) :
	m_pos(pos), 
	m_dir(glm::normalize(dir)),
	m_up(glm::normalize(up)),
	m_viewMat(glm::lookAt(m_pos, m_pos + m_dir, m_up))
{
}

Camera::Camera(const glm::vec3& pos) :
	m_pos(pos), 
	m_dir(0.0f, 0.0f, -1.0f), 
	m_up(0.0f, 1.0f, 0.0f), 
	m_viewMat(glm::lookAt(m_pos, m_pos + m_dir, m_up)) 
{
}

Camera::Camera()
	:m_pos(0.0f,0.0f,1.0f), 
	m_dir(0.0f, 0.0f, -1.0f), 
	m_up(0.0f, 1.0f, 0.0f), 
	m_viewMat(glm::lookAt(m_pos, m_pos+m_dir, m_up))
{
}

void Camera::lookAt(const glm::vec3& pos)
{
	m_dir = glm::normalize(pos - m_pos);
	m_viewMat = glm::lookAt(m_pos, pos, m_up);
}

void Camera::moveTo(const glm::vec3& pos)
{
	m_pos = pos;
	m_viewMat = glm::lookAt(m_pos, m_pos + m_dir, m_up);
}

const glm::mat4& Camera::getViewMat() const
{
	return m_viewMat;
}

glm::mat4* Camera::getViewMatPtr()
{
	return &m_viewMat;
}

glm::vec3 Camera::getPos() const
{
	return m_pos;
}

glm::vec3 Camera::getDir() const
{
	return m_dir;
}

glm::vec3 Camera::getUp() const
{
	return m_up;
}

glm::vec3 Camera::getFront() const
{
	return glm::normalize(glm::cross(-getRight(), m_up));
}

glm::vec3 Camera::getRight() const
{
	return glm::normalize(glm::cross(m_dir, m_up));
}

void Camera::calcMat()
{
	m_dir.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	m_dir.y = sin(glm::radians(pitch));
	m_dir.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	m_dir = glm::normalize(m_dir);

	m_viewMat = glm::lookAt(m_pos, m_pos + m_dir, m_up);
}