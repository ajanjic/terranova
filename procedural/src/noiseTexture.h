#pragma once
#include "glm/glm.hpp"
#include "imgui/imgui.h"
#include "FastNoiseLite.h"

#include <numeric>
#include <random>
#include <string>
#include <functional>
#include <iostream>
#include <algorithm>

#include "texture.h"
#include "utility/util.h"

class NoiseTexture
{
private:
	int m_width;
	int m_height;

	//normalisation values
	float m_maxNoiseVal;
	float m_minNoiseVal;

	char* m_strSeed;
	FastNoiseLite* m_noiseEngine;
	int m_seed;

	unsigned char* m_noiseData;
	float* m_heightData;

	Texture* m_texture = nullptr;

	//callback function on noise generation
	std::function<void()> m_onGenFunction = nullptr;

	void m_calculateTexture();
public:
	NoiseTexture(int w, int h, int seed = 0, float scale = 55.0f, float gain = 0.5f, float lac = 1.67f, int octv = 4, float freq = 1.0f, glm::vec3 offset = {0,0,0});
	~NoiseTexture();

	void generateHeightMap();
	void generate(bool recalculateNoise = true, bool callback = true);

	//callback function which gets called on every generation call
	void setTextureCallback(const std::function<void()>& fnc);
	
public: //getters/setters
	void setSeed(int seed);

	void setSize(int w, int h);
	inline void setScale(float scale) { if (scale < 0) scale = 0.0001f; m_scale = scale; }
	inline void setOctaves(int octv) { m_octaves = octv; m_noiseEngine->SetFractalOctaves(octv); }
	inline void setFrequency(float freq) { m_frequency = freq; m_noiseEngine->SetFrequency(freq); }
	inline void setGain(float gain) { m_gain = gain; m_noiseEngine->SetFractalGain(gain); }    //gain determines how quickly the octave amplitude diminishes for each successive octave
	inline void setLacunarity(float lac) { m_lacunarity = lac; m_noiseEngine->SetFractalLacunarity(lac); }    //lacunarity determines how quickly the frequency increases for each successive octave
	inline void setOffset(glm::vec3 off) { m_offset = off; }

	inline int getWidth() const { return m_width; }
	inline int getHeight() const { return m_height; }

	inline unsigned char* getNoise() const { return m_noiseData; }
	inline float* getHeightMap() const { return m_heightData; }
	inline Texture* getTexture() const { return m_texture; }

//ImGui variables and functions
private:
	//variables used for ImGui controls
	bool m_autoUpdate = false;

	float m_scale;
	int m_octaves;
	float m_frequency;
	float m_gain;
	float m_lacunarity;
	glm::vec3 m_offset;
public:
	void onImGuiRender();
};