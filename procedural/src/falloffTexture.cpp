#include "falloffTexture.h"

FalloffTexture::FalloffTexture(int w, int h, float amt /*= 0.35f*/, float coeff /*= 2.0f*/)
	: m_width(w), m_height(h), m_textureData(new unsigned char[w * h]), m_data(new float[w * h]), m_falloffAmt(amt), m_coeff(coeff), m_widthAux(w), m_heightAux(h)
{
	generate(false);
}

FalloffTexture::~FalloffTexture()
{
	delete[] m_textureData;
	delete[] m_data;
	delete m_texture;
}

void FalloffTexture::setSize(int w, int h)
{
	if ((w == m_width) && (h = m_height))
		return;

	delete[] m_textureData;
	delete[] m_data;
	m_width = m_widthAux = w;
	m_height = m_heightAux = h;
	m_data = new float[w * h];
	m_textureData = new unsigned char[w * h];
}

void FalloffTexture::generate(bool callback /*= true*/)
{
	for (int i = 0; i < m_height; i++)
	{
		for (int j = 0; j < m_width; j++)
		{
			float x = ((float)j / m_width) * 2 - 1;	    //converts the coordinates from [0,1] to [-1,1] after the function 
			float y = ((float)i / m_height) * 2 - 1;	//is applied the segments closer to the value +-1 are a high value
			float sample = (float)std::pow(std::pow(x, 4) + std::pow(y, 4), 1.0f/4);

			if (sample > 1.0f)
				sample = 1.0f;
			float va = std::pow(sample, m_coeff);									            //function which maps [0,1] to a steep curved [0,1]
			float val = va / (va + std::pow(m_falloffAmt - m_falloffAmt * sample, m_coeff));	//

			m_data[i * m_width + j] = val;
		}
	}

	for (int i = 0; i < m_height; i++)
	{
		for (int j = 0; j < m_width; j++)
		{
			m_textureData[i * m_width + j] = (unsigned char)(m_data[i * m_width + j] * 255);
		}
	}

	if (m_texture)
		delete m_texture;
	m_texture = new Texture(m_width, m_height, 1, m_textureData);

	if (m_onGenFunction && callback)
		m_onGenFunction();
}

void FalloffTexture::onImGuiRender()
{
	//ImGui::Begin("Falloff parameters");

	bool checkChange = false;

	ImGui::SliderFloat("Falloff amount", &m_falloffAmt, 0.001f, 10.0f, "%.2f");
	if (ImGui::IsItemDeactivatedAfterEdit())
	{
		checkChange = true;
	}
	ImGui::SliderFloat("Falloff coefficient", &m_coeff, 0.001f, 6.0f, "%.2f");
	if (ImGui::IsItemDeactivatedAfterEdit())
	{
		checkChange = true;
	}

	if (checkChange)
	{
		setSize(m_widthAux, m_heightAux);
		generate();
	}

	ImGui::Image((void*)m_texture->getID(), {256, 256});
	if (ImGui::Button("Generate"))
	{
		setSize(m_width, m_height);
		generate();
	}

	//ImGui::End();
}

void FalloffTexture::setTextureCallback(const std::function<void()>& fnc)
{
	m_onGenFunction = fnc;
}